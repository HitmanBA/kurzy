<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no" />
        <link rel="stylesheet" href="{{ asset('/bower_components/angular-material/angular-material.css') }}">

        <title>Laravel</title>

    </head>
    <body layout="row" ng-app="AngularApp">

        <div ng-view=""></div>

        <script src="{{ asset('/bower_components/angular/angular.js') }}"></script>
        <script src="{{ asset('/bower_components/angular-animate/angular-animate.js') }}"></script>
        <script src="{{ asset('/bower_components/angular-cookies/angular-cookies.js') }}"></script>
        <script src="{{ asset('/bower_components/angular-resource/angular-resource.js') }}"></script>
        <script src="{{ asset('/bower_components/angular-route/angular-route.js') }}"></script>
        <script src="{{ asset('/bower_components/angular-sanitize/angular-sanitize.js') }}"></script>
        <script src="{{ asset('/bower_components/angular-aria/angular-aria.js') }}"></script>
        <script src="{{ asset('/bower_components/angular-messages/angular-messages.js') }}"></script>
        <script src="{{ asset('/bower_components/angular-material/angular-material.js') }}"></script>

        <script src="{{ asset('/scripts/app.js') }}"></script>
        <script src="{{ asset('/scripts/controllers/main.js') }}"></script>
    </body>
</html>
